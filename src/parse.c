/* Copyright (c) 2013, Bastien Dejean
 * Copyright (c) 2021, Jacek Łysiak
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>
#include <ctype.h>
#include "keys.h"
#include "parse.h"

char * strip(char * str)
{
    char *start = lgraph(str);
    char *end = rgraph(str);
    if (end) {
        *(end + 1) = '\0';
    }
    return start;
}

void expand_env_vars(char * str)
{
    char buf[1024] = {0};
    char * varname = NULL;
    char * out = str;
    char * p = str;

    PRINTF("expand from: %s\n", str);
    while (*p) {
        if (varname) {
            if (*p != '}') {
                p++;
                continue;
            }
            *p = 0;
            PRINTF("found variable: %s\n", varname);
            char *envvar = getenv(varname);
            if (envvar) {
                strcat(buf, envvar);
            }
            varname = NULL;
            str = p+1;
        } else if (*p == '$' && *(p+1) == '{') {
            varname = p+2;
            *p = 0;
            strcat(buf, str);
            p++;
        }
        p++;
    }
    strcat(buf, str);
    strcpy(out, buf);
}

void load_config(const char *config_file)
{
	PRINTF("load configuration '%s'\n", config_file);
	FILE *cfg = fopen(config_file, "r");
	if (cfg == NULL)
		err("Can't open configuration file: '%s'.\n", config_file);

	char buf[3 * MAXLEN];
	char chain[MAXLEN] = {0};
	char command[2 * MAXLEN] = {0};
	int offset = 0;
	char first;

	while (fgets(buf, sizeof(buf), cfg) != NULL) {
		first = buf[0];
		if (strlen(buf) < 2 || first == START_COMMENT) {
			continue;
		} else if (first == INCLUDE_SYM){
            char * inc_path = strip(buf+1);
            expand_env_vars(inc_path);
            PRINTF("include keys from: %s\n", inc_path);
            load_config(inc_path);
        } else {
			char *start = lgraph(buf);
			if (start == NULL)
				continue;
			char *end = rgraph(buf);
			*(end + 1) = '\0';

			if (isgraph(first))
				snprintf(chain + offset, sizeof(chain) - offset, "%s", start);
			else
				snprintf(command + offset, sizeof(command) - offset, "%s", start);

			if (*end == PARTIAL_LINE) {
				offset += end - start;
				continue;
			} else {
				offset = 0;
			}

			if (isspace(first) && strlen(chain) > 0 && strlen(command) > 0) {
				process_hotkey(chain, command);
				chain[0] = '\0';
				command[0] = '\0';
			}
		}
	}

	fclose(cfg);
}

void parse_event(xcb_generic_event_t *evt, uint8_t event_type, xcb_keysym_t *keysym, xcb_button_t *button, uint16_t *modfield)
{
	if (event_type == XCB_KEY_PRESS) {
		xcb_key_press_event_t *e = (xcb_key_press_event_t *) evt;
		xcb_keycode_t keycode = e->detail;
		*modfield = e->state;
		*keysym = xcb_key_symbols_get_keysym(symbols, keycode, 0);
		PRINTF("key press %u %u\n", keycode, *modfield);
	} else if (event_type == XCB_KEY_RELEASE) {
		xcb_key_release_event_t *e = (xcb_key_release_event_t *) evt;
		xcb_keycode_t keycode = e->detail;
		*modfield = e->state & ~modfield_from_keycode(keycode);
		*keysym = xcb_key_symbols_get_keysym(symbols, keycode, 0);
		PRINTF("key release %u %u\n", keycode, *modfield);
	} else if (event_type == XCB_BUTTON_PRESS) {
		xcb_button_press_event_t *e = (xcb_button_press_event_t *) evt;
		*button = e->detail;
		*modfield = e->state;
		PRINTF("button press %u %u\n", *button, *modfield);
	} else if (event_type == XCB_BUTTON_RELEASE) {
		xcb_button_release_event_t *e = (xcb_button_release_event_t *) evt;
		*button = e->detail;
		*modfield = e->state;
		PRINTF("button release %u %u\n", *button, *modfield);
	}
}

void process_hotkey(char *hotkey_string, char *command_string)
{
	char hotkey[2 * MAXLEN] = {0};
	char command[2 * MAXLEN] = {0};
	char last_hotkey[2 * MAXLEN] = {0};
	unsigned char num_same = 0;
	chunk_t *hk_chunks = extract_chunks(hotkey_string);
	chunk_t *cm_chunks = extract_chunks(command_string);

#define CHECKCHUNK(s, c) \
	if (c->next == NULL && !c->sequence) { \
		snprintf(s, sizeof(s), "%s", c->text); \
		destroy_chunks(c); \
		c = NULL; \
	}
	CHECKCHUNK(hotkey, hk_chunks)
	CHECKCHUNK(command, cm_chunks)
#undef CHECKCHUNK

	render_next(hk_chunks, hotkey);
	render_next(cm_chunks, command);

	while ((hk_chunks == NULL || hotkey[0] != '\0') && (cm_chunks == NULL || command[0] != '\0')) {

		PRINTF("%s: %s\n", hotkey, command);
		chain_t *chain = make_chain();
		if (parse_chain(hotkey, chain)) {
			hotkey_t *hk = make_hotkey(chain, command);
			add_hotkey(hk);
			if (strcmp(hotkey, last_hotkey) == 0)
				num_same++;
		} else {
			destroy_chain(chain);
		}

		if (hk_chunks == NULL && cm_chunks == NULL)
			break;

		snprintf(last_hotkey, sizeof(last_hotkey), "%s", hotkey);

		render_next(hk_chunks, hotkey);
		render_next(cm_chunks, command);
	}

	if (num_same > 0) {
		int period = num_same + 1;
		int delay = num_same;
		for (hotkey_t *hk = hotkeys_tail; hk != NULL && delay >= 0; hk = hk->prev, delay--)
			hk->cycle = make_cycle(delay, period);
	}

	if (hk_chunks != NULL)
		destroy_chunks(hk_chunks);
	if (cm_chunks != NULL)
		destroy_chunks(cm_chunks);
}

char *get_token(char *dst, char *ign, char *src, char *sep)
{
	size_t len = strlen(src);
	unsigned int i = 0, j = 0, k = 0;
	bool inhibit = false;
	bool found = false;
	while (i < len && !found) {
		if (inhibit) {
			dst[j++] = src[i];
			inhibit = false;
		} else if (src[i] == MAGIC_INHIBIT) {
			inhibit = true;
			if (src[i+1] != MAGIC_INHIBIT && strchr(sep, src[i+1]) == NULL)
				dst[j++] = src[i];
		} else if (strchr(sep, src[i]) != NULL) {
			if (j > 0)
				found = true;
			do {
				if (ign != NULL)
					ign[k++] = src[i];
				i++;
			} while (i < len && strchr(sep, src[i]) != NULL);
			i--;
		} else {
			dst[j++] = src[i];
		}
		i++;
	}
	dst[j] = '\0';
	if (ign != NULL)
		ign[k] = '\0';
	return src + i;
}

void render_next(chunk_t *chunks, char *dest)
{
	if (chunks == NULL)
		return;
	int i = 0;
	bool incr = false;
	for (chunk_t *c = chunks; c != NULL; c = c->next) {
		if (c->sequence) {
			if (!incr) {
				if (c->range_cur < c->range_max) {
					c->range_cur++;
					incr = true;
				} else {
					c->range_cur = 1, c->range_max = 0;
				}
			}
			if (c->advance == NULL) {
				incr = true;
				c->advance = get_token(c->item, NULL, c->text, SEQ_SEP);
			} else if (!incr && c->range_cur > c->range_max) {
				if (c->advance[0] == '\0') {
					c->advance = get_token(c->item, NULL, c->text, SEQ_SEP);
				} else {
					c->advance = get_token(c->item, NULL, c->advance, SEQ_SEP);
					incr = true;
				}
			}
			if (c->range_cur > c->range_max && strlen(c->item) == 3)
				sscanf(c->item, "%c-%c", &c->range_cur, &c->range_max);
			if (c->range_cur <= c->range_max) {
				dest[i++] = c->range_cur;
			} else {
				if (c->item[0] == SEQ_NONE && c->item[1] == '\0')
					continue;
				strcpy(dest + i, c->item);
				i += strlen(c->item);
			}
		} else {
			strcpy(dest + i, c->text);
			i += strlen(c->text);
		}
	}
	dest[i] = '\0';
	if (!incr)
		dest[0] = '\0';
}

chunk_t *extract_chunks(char *s)
{
	size_t len = strlen(s);
	unsigned int i = 0, j = 0;
	bool inhibit = false;
	int num_seq = 0;
	chunk_t *c = make_chunk();
	chunk_t *head = c;
	while (i < len) {
		if (inhibit) {
			c->text[j++] = s[i];
			inhibit = false;
		} else if (s[i] == MAGIC_INHIBIT) {
			inhibit = true;
			if ((s[i+1] != MAGIC_INHIBIT || c->sequence)
					&& s[i+1] != SEQ_BEGIN
					&& s[i+1] != SEQ_END)
				c->text[j++] = s[i];
		} else if (s[i] == SEQ_BEGIN) {
			if (j > 0) {
				c->text[j] = '\0';
				j = 0;
				chunk_t *next = make_chunk();
				c->next = next;
				c = next;
			}
			c->sequence = true;
		} else if (s[i] == SEQ_END) {
			if (c->sequence)
				num_seq++;
			if (j > 0) {
				c->text[j] = '\0';
				j = 0;
				chunk_t *next = make_chunk();
				c->next = next;
				c = next;
			}
			c->sequence = false;
		} else {
			c->text[j++] = s[i];
		}
		i++;
	}
	c->text[j] = '\0';
	return head;
}

chunk_t *make_chunk(void)
{
	chunk_t *c = calloc(1, sizeof(chunk_t));
	c->sequence = false;
	c->advance = NULL;
	c->next = NULL;
	c->range_cur = 1;
	c->range_max = 0;
	return c;
}

void destroy_chunks(chunk_t *chunk)
{
	chunk_t *c = chunk;
	while (c != NULL) {
		chunk_t *next = c->next;
		free(c);
		c = next;
	}
}

bool parse_chain(char *string, chain_t *chain)
{
	char chord[MAXLEN] = {0};
	char name[MAXLEN] = {0};
	char ignored[MAXLEN] = {0};
	xcb_keysym_t keysym = XCB_NO_SYMBOL;
	xcb_button_t button = XCB_NONE;
	uint16_t modfield = 0;
	uint8_t event_type = XCB_KEY_PRESS;
	bool replay_event = false;
	bool lock_chain = false;
	char *outer_advance;
	char *inner_advance;
	for (outer_advance = get_token(chord, ignored, string, LNK_SEP); chord[0] != '\0'; outer_advance = get_token(chord, ignored, outer_advance, LNK_SEP)) {
		for (inner_advance = get_token(name, NULL, chord, SYM_SEP); name[0] != '\0'; inner_advance = get_token(name, NULL, inner_advance, SYM_SEP)) {
			int offset = 0;
			if (name[offset] == REPLAY_PREFIX) {
				replay_event = true;
				offset++;
			}
			if (name[offset] == RELEASE_PREFIX) {
				event_type = XCB_KEY_RELEASE;
				offset++;
			}
			char *nm = name + offset;
			if (!parse_modifier(nm, &modfield) && !parse_keysym(nm, &keysym) && !parse_button(nm, &button)) {
				warn("Unknown keysym name: '%s'.\n", nm);
				return false;
			}
		}
		if (strstr(ignored, GRP_SEP) != NULL)
			lock_chain = true;
		if (button != XCB_NONE)
			event_type = key_to_button(event_type);
		chord_t *c = make_chord(keysym, button, modfield, event_type, replay_event, lock_chain);
		if (c == NULL) {
			return false;
		}
		add_chord(chain, c);
		if (status_fifo != NULL) {
			snprintf(c->repr, sizeof(c->repr), "%s", chord);
		}
		keysym = XCB_NO_SYMBOL;
		button = XCB_NONE;
		modfield = 0;
		event_type = XCB_KEY_PRESS;
		replay_event = false;
		lock_chain = false;
	}
	return true;
}

bool parse_keysym(char *name, xcb_keysym_t *keysym)
{
	for (unsigned int i = 0; i < LENGTH(nks_dict); i++) {
		keysym_dict_t nks = nks_dict[i];
		if (strcmp(name, nks.name) == 0) {
			*keysym = nks.keysym;
			return true;
		}
	}
	return false;
}

bool parse_button(char *name, xcb_button_t *butidx)
{
	/* X handles up to 24 buttons */
	return (sscanf(name, "button%" SCNu8, butidx) == 1);
}

bool parse_modifier(char *name, uint16_t *modfield)
{
	if (strcmp(name, "shift") == 0) {
		*modfield |= XCB_MOD_MASK_SHIFT;
		return true;
	} else if (strcmp(name, "control") == 0 || strcmp(name, "ctrl") == 0) {
		*modfield |= XCB_MOD_MASK_CONTROL;
		return true;
	} else if (strcmp(name, "alt") == 0) {
		*modfield |= (modfield_from_keysym(Alt_L) | modfield_from_keysym(Alt_R));
		return true;
	} else if (strcmp(name, "super") == 0) {
		*modfield |= (modfield_from_keysym(Super_L) | modfield_from_keysym(Super_R));
		return true;
	} else if (strcmp(name, "hyper") == 0) {
		*modfield |= (modfield_from_keysym(Hyper_L) | modfield_from_keysym(Hyper_R));
		return true;
	} else if (strcmp(name, "meta") == 0) {
		*modfield |= (modfield_from_keysym(Meta_L) | modfield_from_keysym(Meta_R));
		return true;
	} else if (strcmp(name, "mode_switch") == 0) {
		*modfield |= modfield_from_keysym(Mode_switch);
		return true;
	} else if (strcmp(name, "mod1") == 0) {
		*modfield |= XCB_MOD_MASK_1;
		return true;
	} else if (strcmp(name, "mod2") == 0) {
		*modfield |= XCB_MOD_MASK_2;
		return true;
	} else if (strcmp(name, "mod3") == 0) {
		*modfield |= XCB_MOD_MASK_3;
		return true;
	} else if (strcmp(name, "mod4") == 0) {
		*modfield |= XCB_MOD_MASK_4;
		return true;
	} else if (strcmp(name, "mod5") == 0) {
		*modfield |= XCB_MOD_MASK_5;
		return true;
	} else if (strcmp(name, "lock") == 0) {
		*modfield |= XCB_MOD_MASK_LOCK;
		return true;
	} else if (strcmp(name, "any") == 0) {
		*modfield |= XCB_MOD_MASK_ANY;
		return true;
	}
	return false;
}

bool parse_fold(char *string, char *folded_string)
{
	if (strchr(string, SEQ_BEGIN) != NULL && strrchr(string, SEQ_END) != NULL) {
		snprintf(folded_string, strlen(string), "%s", string);
		return true;
	}
	return false;
}

uint8_t key_to_button(uint8_t event_type)
{
	if (event_type == XCB_KEY_PRESS)
		return XCB_BUTTON_PRESS;
	else if (event_type == XCB_KEY_RELEASE)
		return XCB_BUTTON_RELEASE;
	return event_type;
}

void get_standard_keysyms(void)
{
#define GETKS(X) \
	if (!parse_keysym(#X, &X)) \
		warn("Couldn't retrieve keysym for '%s'.\n", #X); \
	else \
		PRINTF("keysym for '%s' is 0x%X.\n", #X, X);
	GETKS(Alt_L)
	GETKS(Alt_R)
	GETKS(Super_L)
	GETKS(Super_R)
	GETKS(Hyper_L)
	GETKS(Hyper_R)
	GETKS(Mode_switch)
	GETKS(Num_Lock)
	GETKS(Scroll_Lock)
#undef GETKS
}

void get_lock_fields(void)
{
	num_lock = modfield_from_keysym(Num_Lock);
	caps_lock = XCB_MOD_MASK_LOCK;
	scroll_lock = modfield_from_keysym(Scroll_Lock);
	PRINTF("lock fields %u %u %u\n", num_lock, caps_lock, scroll_lock);
}

int16_t modfield_from_keysym(xcb_keysym_t keysym)
{
	uint16_t modfield = 0;
	xcb_keycode_t *keycodes = NULL;
	if ((keycodes = keycodes_from_keysym(keysym)) != NULL) {
		for (xcb_keycode_t *k = keycodes; *k != XCB_NO_SYMBOL; k++)
			modfield |= modfield_from_keycode(*k);
	}
	free(keycodes);
	return modfield;
}

int16_t modfield_from_keycode(xcb_keycode_t keycode)
{
	uint16_t modfield = 0;
	xcb_keycode_t *mod_keycodes = NULL;
	xcb_get_modifier_mapping_reply_t *reply = NULL;
	if ((reply = xcb_get_modifier_mapping_reply(dpy, xcb_get_modifier_mapping(dpy), NULL)) != NULL && reply->keycodes_per_modifier > 0) {
		if ((mod_keycodes = xcb_get_modifier_mapping_keycodes(reply)) != NULL) {
			unsigned int num_mod = xcb_get_modifier_mapping_keycodes_length(reply) / reply->keycodes_per_modifier;
			for (unsigned int i = 0; i < num_mod; i++) {
				for (unsigned int j = 0; j < reply->keycodes_per_modifier; j++) {
					xcb_keycode_t mkc = mod_keycodes[i * reply->keycodes_per_modifier + j];
					if (mkc == XCB_NO_SYMBOL)
						continue;
					if (keycode == mkc)
						modfield |= (1 << i);
				}
			}

		}
	}
	free(reply);
	return modfield;
}

xcb_keycode_t *keycodes_from_keysym(xcb_keysym_t keysym)
{
	xcb_setup_t const *setup;
	unsigned int num = 0;
	xcb_keycode_t *result = NULL, *result_np = NULL;

	if ((setup = xcb_get_setup(dpy)) != NULL) {
		xcb_keycode_t min_kc = setup->min_keycode;
		xcb_keycode_t max_kc = setup->max_keycode;

		/* We must choose a type for kc other than xcb_keycode_t whose size
		 * is 1, otherwise, since max_kc will most likely be 255, if kc == 255,
		 * kc++ would be 0 and the outer loop would start over ad infinitum */
		for(unsigned int kc = min_kc; kc <= max_kc; kc++)
			for(unsigned int col = 0; col < KEYSYMS_PER_KEYCODE; col++) {
				xcb_keysym_t ks = xcb_key_symbols_get_keysym(symbols, kc, col);
				if (ks == keysym) {
					num++;
					result_np = realloc(result, sizeof(xcb_keycode_t) * (num + 1));
					if (result_np == NULL) {
						free(result);
						return NULL;
					}
					result = result_np;
					result[num - 1] = kc;
					result[num] = XCB_NO_SYMBOL;
					break;
				}
			}
	}
	return result;
}
